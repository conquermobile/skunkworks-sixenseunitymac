Conquer Mobile Skunkworks Project
Sixense Unity Mac Driver

Repack of the Sixense library for Unity 4 Pro
By: Aaron Hilton - CTO of Conquer Mobile  (aaron@conquermobile.com)

Ready to Install Instructions:
• Copy the contents of Plugins to your project's ../Assets/Plugins folder
• Copy the contents of SexenseInput to your project's ../Assets folder
• Open the SixenseInput prefab in your Unity project
• Open the SixenseInput/DemoScenes/SixenseHands scene
• Mult-select & copy: Left Hand, Right Hand, and SixenseInput
• Switch to your own project's scene
• Paste in the Hands and SixenseInput
• Re-parent both Hands to your camera, and carefully position them in front and below the camera, all facing the same way
• Enjoy!

Build & Discovery Instructions:
• Open project
• Build.  Everything should be self-managed.
• Right-click on Project Tree -> Products -> sixense.bundle
     select Show in Finder
• Copy bundle to your Assets/Plugins folder
  

NOTE look inside:
   project sixense -> target sixense -> Build Phases -> Run Script
   You'll see how libsixense.dylib is being retooled to use the @loader_path,
   so the plugin should just work wherever it's stored on Mac.

Known issues:
• Trying to resolve why Oculus Mac drivers conflict with Sixense driver init
• Unity crashes on second run  (does not like stop & go x2)


Last Updated: 2013-12-01
